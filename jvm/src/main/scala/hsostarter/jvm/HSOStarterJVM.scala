package hsostarter.jvm

import cats.effect._
import cats.implicits._
import hsostarter.jvm.config.{AppConfig, SlickConfig, SyncConfig}
import hsostarter.jvm.data.db.{SlickPgProfile, Transactor}
import hsostarter.jvm.http._
import org.flywaydb.core.Flyway
import org.http4s.implicits._
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.{CORS, CORSConfig}
import pureconfig.ConfigSource
import scribe.{Level, Logger}
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext

object HSOStarterJVM extends IOApp {
  val originConfig = CORSConfig(
    anyOrigin = true,
    allowCredentials = true,
    maxAge = 1.day.toSeconds,
  )

  def sysEnvOrDefault(
    envName: String,
    default: String,
    warnIfNotFound: Boolean = false,
  ): String = {
    val env: Option[String] = sys.env.get(envName)

    if (env.isEmpty && warnIfNotFound) {
      println(
        s"[sysEnvOrDefault] Environment variable '$envName' not found, using default value '$default' instead",
      )
    }

    env.getOrElse(default)
  }

  override def run(args: List[String]): IO[ExitCode] =
    for {
      // base directory for all non-static assets
      dynamicAssetsHost <- IO(
        sysEnvOrDefault(
          "DYNAMIC_HOST",
          "http://localhost:5000",
          warnIfNotFound = true,
        ),
      )
      _ <- IO(
        println(s"Starting server with DYNAMIC_HOST=$dynamicAssetsHost"),
      )
      _ <- IO(
        Logger.root
          .clearHandlers().clearModifiers()
          .withHandler(minimumLevel = Some(Level.Debug))
          .replace(),
      )
      appConfig <- SyncConfig.read[IO, AppConfig](ConfigSource.default)
      slickNativeConfig <- IO(
        DatabaseConfig.forConfig[SlickPgProfile]("slick.dbs.default"),
      )
      slickRawConfig = slickNativeConfig.config
      slickConfig <- SyncConfig.read[IO, SlickConfig](
        ConfigSource.fromConfig(slickRawConfig),
      )
      flyway =
        Flyway
          .configure()
          .dataSource(
            slickConfig.db.url,
            slickConfig.db.user,
            slickConfig.db.password,
          )
          .baselineOnMigrate(true)
          .outOfOrder(true)
          .ignoreMissingMigrations(true)
          .load()
      _ <- IO(flyway.migrate())
      exitCode <- app(appConfig, slickNativeConfig, dynamicAssetsHost)
        .use(_ => IO.never)
        .as(ExitCode.Success)
    } yield exitCode

  private def app(
    appConfig: AppConfig,
    slickNativeConfig: DatabaseConfig[_ <: JdbcProfile],
    dynamicAssetsHost: String,
  ): Resource[IO, Server[IO]] =
    for {
      blocker <- Blocker[IO]
      staticEndpoints = new StaticEndpoints[IO](appConfig.assets, blocker)
      transactor <- Transactor.fromDatabaseConfig[IO](slickNativeConfig)
      httpApp = (
        staticEndpoints.endpoints() <+>
          new AlbumRoutes(transactor, staticEndpoints, dynamicAssetsHost).routes
      ).orNotFound
      server <- BlazeServerBuilder[IO](ExecutionContext.global)
        .bindHttp(appConfig.http.port, appConfig.http.host)
        .withHttpApp(CORS(httpApp, originConfig))
        .resource
    } yield server

}
