package hsostarter.jvm.http

import cats.syntax.functor._
import cats.syntax.flatMap._
import cats.effect.{Blocker, Sync}
import hsostarter.jvm.data.db.{AlbumDBIO, TrackDBIO, Transactor}
import hsostarter.models
import hsostarter.models.{Album, AlbumData, PagerData, SearchQuery, TracksQuery}
import org.http4s.{HttpRoutes, Request, Response, StaticFile, Uri}
import mouse.string._
import cats.effect.{Blocker, ContextShift, Effect}
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.Location

object AlbumIdVar {
  def unapply(str: String): Option[Album.Id] =
    str.parseIntOption.map(models.Id.apply[Album, Int])
}

final class AlbumRoutes[F[_]: Sync](
  transactor: Transactor[F],
  staticEndpoints: StaticEndpoints[F],
  dynamicAssetsHost: String,
) extends Http4sDsl[F] {
  def routes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      case request @ POST -> Root / "getAlbums" =>
        for {
          pager  <- request.as[PagerData]
          albums <- transactor.transact(AlbumDBIO.all(pager.page, pager.limit))
          result <- Ok(albums.toList)
        } yield result

      case request @ POST -> Root / "search" =>
        for {
          search <- request.as[SearchQuery]
          albums <- transactor.transact(
            AlbumDBIO.search(
              search.query,
              search.pager.page,
              search.pager.limit,
            ),
          )
          result <- Ok(albums.toList.map(_.applyHost(dynamicAssetsHost)))
        } yield result

      case request @ POST -> Root / "getTracks" =>
        for {
          params <- request.as[TracksQuery]
          tracks <- transactor.transact(TrackDBIO.byAlbumId(params.albumId))
          result <- Ok(tracks.toList.map(_.applyHost(dynamicAssetsHost)))
        } yield result
    }
}
