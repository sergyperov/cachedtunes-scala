package hsostarter.jvm.data.db

import hsostarter.jvm.data.db.SlickPgProfile.api._
import hsostarter.models.{Author, AuthorData}
import slick.dbio.DBIO
import slick.lifted.{MappedProjection, ProvenShape}

object AuthorDBIO {
  type AuthorId = hsostarter.models.Id[Author, Int]

  final class DataTable(tag: Tag) extends Table[Author](tag, "author") {

    def id = column[AuthorId]("id", O.AutoInc, O.PrimaryKey)
    def name = column[String]("name")
    def avatarSrc = column[String]("avatar_src")

    def data: MappedProjection[Option[AuthorData], (String, String)] =
      (name, avatarSrc) <> (
        t => Some(AuthorData(t._1, t._2)),
        _.map(d => (d.name, d.avatarSrc))
      )

    def * : ProvenShape[Author] =
      (
        id,
        data,
      ) <>
        ((Author.apply _).tupled, Author.unapply)
  }

  object Queries {

    val table = TableQuery(new DataTable(_))
    def schema = table.schema

    val data = Compiled(table.map(_.data))

    val byId = Compiled((id: Rep[Author.Id]) => table.filter(_.id === id))
  }

  def byId(id: Author.Id): DBIO[Option[Author]] =
    Queries.byId(id).result.headOption

  def update(note: Author): DBIO[Int] =
    Queries.table.update(note)

  def delete(id: Author.Id): DBIO[Int] =
    Queries.byId(id).delete
}
