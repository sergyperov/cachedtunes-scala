package hsostarter.jvm.data.db

object DbSchema {
  lazy val createStatementsList =
    List(
      AlbumDBIO.Queries.schema.createStatements.toList,
      AuthorDBIO.Queries.schema.createStatements.toList,
      TrackDBIO.Queries.schema.createStatements.toList,
    ).flatten

}
