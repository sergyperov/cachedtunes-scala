package hsostarter.jvm.data.db

import hsostarter.jvm.data.db.SlickPgProfile.api._
import hsostarter.models.{Album, AlbumData, Author, AuthorData}
import slick.dbio.DBIO
import slick.lifted.{ForeignKeyQuery, MappedProjection, ProvenShape}

object AlbumDBIO {
  final class DataTable(tag: Tag) extends Table[Album](tag, "album") {

    def id = column[Album.Id]("id", O.AutoInc, O.PrimaryKey)
    def title = column[String]("title")
    def coverSrc = column[String]("cover_src")

    def authorId = column[Author.Id]("author_id");
    def author: ForeignKeyQuery[AuthorDBIO.DataTable, Author] =
      foreignKey("SUP_FK", authorId, AuthorDBIO.Queries.table)(
        _.id,
        onUpdate = ForeignKeyAction.SetNull,
        onDelete = ForeignKeyAction.SetNull,
      )

    def reference: MappedProjection[Option[AuthorData], Author.Id] =
      authorId <> (
        _ => None,
        _ => None,
      )

    def data =
      (
        title,
        coverSrc,
        reference,
      ) <>
        ((AlbumData.apply _).tupled, AlbumData.unapply)

    def * : ProvenShape[Album] =
      (
        id,
        authorId,
        data,
      ) <>
        ((Album.apply _).tupled, Album.unapply)
  }

  object Queries {

    val table = TableQuery(new DataTable(_))
    def schema = table.schema

    val data = Compiled(table.map(_.data))

    val byId = Compiled((id: Rep[Album.Id]) => table.filter(_.id === id))

    val allByPages = Compiled {
      (d: ConstColumn[Long], t: ConstColumn[Long], q: ConstColumn[String]) =>
        val innerJoinRes = for {
          (album, author) <-
            table join AuthorDBIO.Queries.table on (_.authorId === _.id)
        } yield (album, author)

        val filteredByTitle = for {
          byTitle <- innerJoinRes if byTitle._1.title like q
        } yield byTitle

        filteredByTitle.drop(d).take(t).map { l =>
          val albumData = (
            l._1.title,
            l._1.coverSrc,
            l._2.data,
          ) <>
            ((AlbumData.apply _).tupled, AlbumData.unapply)

          (
            l._1.id,
            l._1.authorId,
            albumData,
          ) <>
            ((Album.apply _).tupled, Album.unapply)
        }

    }
  }

  def all(limit: Int, offset: Int): DBIO[Seq[Album]] =
    Queries.allByPages(limit * offset, offset, "%").result

  def search(query: String, limit: Int, offset: Int): DBIO[Seq[Album]] =
    Queries.allByPages(limit * offset, offset, s"%$query%").result

  def byId(id: Album.Id): DBIO[Option[Album]] =
    Queries.byId(id).result.headOption

  def insert(data: AlbumData): DBIO[Album.Id] =
    Queries.data.returning(Queries.table.map(_.id)) += data

  def update(note: Album): DBIO[Int] =
    Queries.table.update(note)

  def delete(id: Album.Id): DBIO[Int] =
    Queries.byId(id).delete
}
