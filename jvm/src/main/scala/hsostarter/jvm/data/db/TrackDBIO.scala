package hsostarter.jvm.data.db

import hsostarter.jvm.data.db.SlickPgProfile.api._
import hsostarter.models.{Album, Track}
import slick.dbio.DBIO
import slick.lifted.{ForeignKeyQuery, ProvenShape}

object TrackDBIO {
  final class DataTable(tag: Tag) extends Table[Track](tag, "track") {

    def id = column[Track.Id]("id", O.AutoInc, O.PrimaryKey)
    def position = column[Int]("position")
    def title = column[String]("title")
    def duration = column[Int]("duration")
    def source = column[String]("audio_src")

    def albumId = column[Album.Id]("album_id");
    def album: ForeignKeyQuery[AlbumDBIO.DataTable, Album] =
      foreignKey("SUP_FK", albumId, AlbumDBIO.Queries.table)(
        _.id,
        onUpdate = ForeignKeyAction.SetNull,
        onDelete = ForeignKeyAction.SetNull,
      )

    def * : ProvenShape[Track] =
      (
        id,
        albumId,
        position,
        title,
        duration,
        source,
      ) <>
        ((Track.apply _).tupled, Track.unapply)
  }

  object Queries {

    val table = TableQuery(new DataTable(_))
    def schema = table.schema

    val byAlbumId =
      Compiled((id: Rep[Album.Id]) => table.filter(_.albumId === id))

    val byId = Compiled((id: Rep[Track.Id]) => table.filter(_.id === id))
  }

  def byAlbumId(id: Album.Id): DBIO[Seq[Track]] =
    Queries.byAlbumId(id).result

  def byId(id: Track.Id): DBIO[Option[Track]] =
    Queries.byId(id).result.headOption
}
