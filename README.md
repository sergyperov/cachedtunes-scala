# Пример http4s + Slick + circe + OutWatch

## Демо

Приложение разворачивается на Heroku посредством GitLab CI:

https://http4s-slick-outwatch-starter.herokuapp.com/

## Настройка окружения для локальной разработки

- Установить необходимый инструментарий:
    - [Git](https://git-scm.com/)
    - [sbt](https://www.scala-sbt.org/)
    - [PostgreSQL](https://www.postgresql.org/)
- Склонировать репозиторий.

## Запуск локальной версии для разработки

Для начала работы необходимо запустить [sbt](https://www.scala-sbt.org/) в папке
проекта. Далее указываются команды интерактивного режима sbt.

Для разработки следует использовтаь локальную БД PostgreSQL, подключение к 
которой настраивается в конфигурации приложения. Чтобы не разглашать пароли 
файлы `*.local.conf` игнорируются в репозитории и остаются только на локальной
машине. Следует создать такой файл на основе 
[application.local.sample.conf](src/main/resources/application.local.sample.conf).

### Серверная часть (JVM)

Сервер запускается через
[sbt-revolver](https://github.com/spray/sbt-revolver). Для запуска выполнить

```
reStart --- -Dconfig.resource=application.local.conf
```

При изменении серверного кода следует выполнить команду вновь или запустить
отслеживание изменений:

```
~reStart --- -Dconfig.resource=application.local.conf
``` 

### Клиентская часть (JS)

Для первоначальной сборки js-зависимостей и после каждого изменении настройки 
`npmDependencies` выполнить

```
fastOptJS::webpack
```

Для сборки клиентского кода выполнть

```
fastOptJS
```

Для открытия головной страницы для локальной разработки в браузере выполнить

```
openDev
```

При изменении клиентского кода следует выполнить `fastOptJS` вновь или запустить
отслеживание изменений:

```
~fastOptJS
``` 

## Работа с БД

Для работы с БД используется [Flyway](https://flywaydb.org/). Все изменения 
схемы должны проводиться при помощи миграций. Чтобы правильным образом
генерировать номера миграций, следует пользоваться шаблоном, см. ниже.

Для генерации SQL-схемы текущей базы нужно внести ссылки на все таблицы в
[`DbSchema`](jvm/src/main/scala/hsostarter/jvm/data/db/DbSchema.scala) 
и выполнить в sbt:

```
project hsostarterJVM
runMain hsostarter.jvm.DbSchemaDumper
```

Для генерации шаблона новой миграции для текущего момента выполнить в sbt:

```
project hsostarterJVM
runMain hsostarter.jvm.MigrationInitializer
```

Для возврата в корневой проект

```
project /
```

## Пример работы с API

Отправка запроса на создание заметки (`Note`) в локально запущенное приложение (на `http://127.0.0.1:8080`)

```
> curl -X POST -d "{\"instant\":\"2011-11-10T20:00:00Z\",\"text\":\"test\"}" http://127.0.0.1:8080/notes
1
> curl http://127.0.0.1:8080/notes/1
{"id":1,"data":{"instant":"2011-11-10T20:00:00Z","text":"test"}}
```
