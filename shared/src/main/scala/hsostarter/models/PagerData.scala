package hsostarter.models

import cats.effect.Sync
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

@JsonCodec final case class PagerData(
  page: Int,
  limit: Int,
)

object PagerData {
  // JS encoders/decoders
  implicit val pagerDataEncoder: Encoder[PagerData] = deriveEncoder
  implicit val pagerDataDecoder: Decoder[PagerData] = deriveDecoder

  // JVM encoders/decoders
  implicit def AlbumDataEntityDecoder[F[_]: Sync]: EntityDecoder[F, PagerData] =
    jsonOf[F, PagerData]
}
