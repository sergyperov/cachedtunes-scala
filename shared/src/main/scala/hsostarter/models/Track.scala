package hsostarter.models

import cats.effect.Sync
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

@JsonCodec final case class Track(
  id: Track.Id,
  albumId: Album.Id,
  position: Int,
  title: String,
  duration: Int,
  source: String,
) {
  def applyHost(host: String): Track =
    copy(source = host.concat(source))
}

object Track {
  type Id = hsostarter.models.Id[Track, Int]

  // JS encoders/decoders
  implicit val trackEncoder: Encoder[Track] = deriveEncoder
  implicit val trackDecoder: Decoder[Track] = deriveDecoder

  // JVM encoders/decoders
  implicit def optionTrackEntityEncoder[F[_]]: EntityEncoder[F, Option[Track]] =
    jsonEncoderOf[F, Option[Track]]

  implicit def listTrackEntityEncoder[F[_]]: EntityEncoder[F, List[Track]] =
    jsonEncoderOf[F, List[Track]]

  implicit def trackEntityDecoder[F[_]: Sync]: EntityDecoder[F, Track] =
    jsonOf[F, Track]
}
