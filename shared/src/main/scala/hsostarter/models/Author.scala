package hsostarter.models

import cats.effect.Sync
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

@JsonCodec final case class AuthorData(
  name: String,
  avatarSrc: String,
)

object AuthorData {
  // JS encoders/decoders
  implicit val authorEncoder: Encoder[AuthorData] = deriveEncoder
  implicit val authorDecoder: Decoder[AuthorData] = deriveDecoder

  // JVM encoders/decoders
  implicit def authorDataEntityDecoder[F[_]: Sync]
    : EntityDecoder[F, AuthorData] =
    jsonOf[F, AuthorData]

  implicit def authorEntityDecoder[F[_]: Sync]
    : EntityEncoder[F, List[AuthorData]] =
    jsonEncoderOf[F, List[AuthorData]]

  implicit def optionAuthorDataEntityEncoder[F[_]]
    : EntityEncoder[F, Option[AuthorData]] =
    jsonEncoderOf[F, Option[AuthorData]]

  implicit def optionAuthorDataEntityDecoder[F[_]: Sync]
    : EntityDecoder[F, Option[AuthorData]] =
    jsonOf[F, Option[AuthorData]]
}

@JsonCodec final case class Author(
  id: Author.Id,
  data: Option[AuthorData],
)

object Author {
  type Id = hsostarter.models.Id[Author, Int]

  // JS encoders/decoders
  implicit val albumEncoder: Encoder[Author] = deriveEncoder
  implicit val albumDecoder: Decoder[Author] = deriveDecoder

  // JVM encoders/decoders
  implicit def optionAuthorEntityEncoder[F[_]]
    : EntityEncoder[F, Option[Author]] =
    jsonEncoderOf[F, Option[Author]]

  implicit def listAuthorEntityEncoder[F[_]]: EntityEncoder[F, List[Author]] =
    jsonEncoderOf[F, List[Author]]

  implicit def authorEntityDecoder[F[_]: Sync]: EntityDecoder[F, Author] =
    jsonOf[F, Author]
}
