package hsostarter.models

import cats.effect.Sync
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

@JsonCodec final case class Album(
  id: Album.Id,
  authorId: Author.Id,
  data: AlbumData,
) {
  def applyHost(host: String): Album =
    copy(data = data.applyHost(host))
}

object Album {
  type Id = hsostarter.models.Id[Album, Int]

  // JS encoders/decoders
  implicit val albumEncoder: Encoder[Album] = deriveEncoder
  implicit val albumDecoder: Decoder[Album] = deriveDecoder

  // JVM encoders/decoders
  implicit def optionAlbumEntityEncoder[F[_]]: EntityEncoder[F, Option[Album]] =
    jsonEncoderOf[F, Option[Album]]

  implicit def listAlbumEntityEncoder[F[_]]: EntityEncoder[F, List[Album]] =
    jsonEncoderOf[F, List[Album]]

  implicit def noteEntityDecoder[F[_]: Sync]: EntityDecoder[F, Album] =
    jsonOf[F, Album]
}

@JsonCodec final case class AlbumData(
  title: String,
  coverSrc: String,
  author: Option[AuthorData],
) {
  def applyHost(host: String): AlbumData =
    copy(coverSrc = host.concat(coverSrc))
}

object AlbumData {
  implicit val albumEncoder: Encoder[AlbumData] = deriveEncoder
  implicit val albumDecoder: Decoder[AlbumData] = deriveDecoder
}
