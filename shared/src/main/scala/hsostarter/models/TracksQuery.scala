package hsostarter.models

import cats.effect.Sync
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonOf}
import org.http4s.{EntityDecoder}

@JsonCodec final case class TracksQuery(
  albumId: Album.Id,
)

object TracksQuery {
  // JS encoders/decoders
  implicit val tracksQueryEncoder: Encoder[TracksQuery] = deriveEncoder
  implicit val tracksQueryDecoder: Decoder[TracksQuery] = deriveDecoder

  // JVM encoders/decoders
  implicit def AlbumDataEntityDecoder[F[_]: Sync]
    : EntityDecoder[F, TracksQuery] =
    jsonOf[F, TracksQuery]
}
