package hsostarter.models

import cats.effect.Sync
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

@JsonCodec final case class SearchQuery(
  query: String,
  pager: PagerData,
)

object SearchQuery {
  // JS encoders/decoders
  implicit val searchQueryEncoder: Encoder[SearchQuery] = deriveEncoder
  implicit val searchQueryDecoder: Decoder[SearchQuery] = deriveDecoder

  // JVM encoders/decoders
  implicit def SearchQueryEntityDecoder[F[_]: Sync]
    : EntityDecoder[F, SearchQuery] =
    jsonOf[F, SearchQuery]
}
