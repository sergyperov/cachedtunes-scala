package hsostarter.js.components

import monix.reactive.Observable
import outwatch.dom.dsl._
import outwatch.dom.{Handler, VDomModifier}

final case class HeaderComponent(
  searchInputHandler: Handler[String],
) {
  val node: Observable[VDomModifier] =
    for {
      searchInput <- searchInputHandler
    } yield
      div(
        cls := "Header",
        div(
          cls := "HeaderLogoWrapper",
          img(
            cls := "HeaderLogoImg",
            alt := "cachetunes logo",
            src := "assets/styles/icons/logo.svg",
          ),
          label(
            cls := "HeaderLogoLabel",
            "cachedtunes",
          ),
        ),
        div(
          cls := "HeaderSearch",
          input(
            typ := "text",
            placeholder := "Search...",
            value <-- searchInputHandler,
            onInput.value --> searchInputHandler,
          ),
        ),
        button(
          cls := "OptionsButton",
          img(
            cls := "OptionsButtonImg",
            alt := "cachedtunes logo",
            src := "assets/styles/icons/settings.svg",
          ),
          label(
            cls := "OptionsButtonLabel",
            "Settings",
          ),
        ),
      )
}
