package hsostarter.js.components

import cats.effect.IO
import outwatch.dom.{Handler, VDomModifier, VNode}
import outwatch.dom.dsl._
import cats.implicits._
import hsostarter.js.data.db.DatabaseManager
import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable

final case class AlbumsGridComponent(
  albumsDataStream: Observable[List[AlbumCoverComponent]],
  gridTitle: Observable[String],
  dbManager: DatabaseManager,
  gridDisplayModeStream: Observable[String],
) {
  val node: Observable[VDomModifier] =
    for {
      albumsData <- albumsDataStream
    } yield
      div(
        cls := "Contents",
        label(
          cls := (if (albumsData.nonEmpty) {
                    "MusicLibraryWrapper__Title"
                  } else {
                    "MusicLibraryWrapper__Title hidden"
                  }),
          //display := albumsData.nonEmpty.toString,
          gridTitle,
        ),
        if (albumsData.nonEmpty) {
          div(
            cls := "MusicLibraryWrapper",
            albumsData.map(_.node),
          )
        } else {
          div(
            cls := "EmptyState",
            gridDisplayModeStream.map({
              case "local" =>
                "There are no saved compositions yet. You can search for some and add them to library!"
              case "remote" => "No music was found by your request"
            }),
          )
        },
      )
}
