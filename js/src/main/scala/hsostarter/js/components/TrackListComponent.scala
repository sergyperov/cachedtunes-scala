package hsostarter.js.components

import hsostarter.js.components.TrackListComponent.TrackListProps
import hsostarter.js.data.db.DatabaseManager
import hsostarter.js.http.{Requests}
import hsostarter.models.{Album, Track}
import monix.reactive.Observable
import outwatch.dom._
import outwatch.dom.dsl._

final case class TrackListComponent(
  albumAndTracksStream: Observable[Option[TrackListProps]],
  audioPlayerComponentStream: Handler[Option[AudioPlayerState]],
  dbManager: DatabaseManager,
) {
  def onTrackClick(track: Track, album: Album): Unit =
    audioPlayerComponentStream.onNext(
      Some(AudioPlayerState(track, album.data, isPlaying = true)),
    )

  def toMinutes(seconds: Int): String =
    s"${seconds / 60}:${(seconds % 60).toString.reverse.padTo(2, '0').reverse}"

  def hasInLibrary(album: Album): Observable[Boolean] =
    dbManager.albumsListHandler
      .map(
        _.exists(_.id == album.id),
      )

  val node: Observable[VDomModifier] =
    for {
      albumAndTracksOption <- albumAndTracksStream
    } yield
      div(
        cls := (if (albumAndTracksOption.isEmpty) { "TrackList" }
                else { "TrackList active" }),
        albumAndTracksOption match {
          case Some((album, trackList)) =>
            VDomModifier(
              div(
                cls := "TrackList__Header",
                div(
                  cls := "TrackList__Header__Artwork",
                  img(
                    src <-- hasInLibrary(album).flatMap({
                      case true =>
                        dbManager.Query.getAlbumCoverBlobUrl(album.id.value)
                      case _ =>
                        Observable(Requests.Urls.albumCover(album))
                    }),
                  ),
                ),
                div(
                  cls := "TrackList__Header__MetaData",
                  label(
                    cls := "TrackList__Header__MetaData__AlbumTitle",
                    album.data.title,
                  ),
                  label(
                    cls := "TrackList__Header__MetaData__AlbumAuthor",
                    album.data.author.map(_.name).get,
                  ),
                  button(
                    cls := "TrackList__Header__MetaData__AddToLibrary",
                    hasInLibrary(album).map({
                      case true => "In Library"
                      case _    => "Add to Library"
                    }),
                    onClick.foreach { _ =>
                      dbManager.Query.addAlbum(album).unsafeRunAsyncAndForget()
                    },
                  ),
                ),
              ),
              div(
                cls := "TrackList__Contents",
                trackList
                  .sortWith((trackA, trackB) =>
                    trackA.position < trackB.position,
                  ).map(track =>
                    button(
                      cls := "TrackList__Contents__Track",
                      label(
                        cls := "TrackList__Contents__Track__Position",
                        track.position,
                      ),
                      label(
                        cls := "TrackList__Contents__Track__Title",
                        track.title,
                      ),
                      label(
                        cls := "TrackList__Contents__Track__Duration",
                        toMinutes(track.duration),
                      ),
                      onClick.foreach(_ => onTrackClick(track, album)),
                    ),
                  ),
              ),
              AudioPlayerComponent(
                audioPlayerComponentStream,
              ).node,
            )
          case None => div()
        },
      )
}

object TrackListComponent {
  type TrackListProps = (Album, List[Track])
}
