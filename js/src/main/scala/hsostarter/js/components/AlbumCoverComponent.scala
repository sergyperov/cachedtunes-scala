package hsostarter.js.components

import hsostarter.js.data.db.DatabaseManager
import hsostarter.js.http.{Requests}
import hsostarter.models.Album
import monix.reactive.Observable
import outwatch.dom._
import outwatch.dom.dsl._
import monix.execution.Scheduler.Implicits.global

final case class AlbumCoverComponent(
  metaDataHandler: Observable[Album],
  sourceStateStream: Observable[String],
  dbManager: DatabaseManager,
  currentAlbumHandler: Handler[Option[Album]],
) {
  val clickHndlr = for {
    currentAlbum <- metaDataHandler
  } yield currentAlbumHandler.onNext(Some(currentAlbum))

  val node: VNode =
    button(
      cls := "AlbumCover",
      img(
        cls := "AlbumCoverImg",
        alt <-- metaDataHandler.map(_.data.title),
        src <-- sourceStateStream
          .zip(metaDataHandler).flatMap({
            case ("local", album) =>
              dbManager.Query.getAlbumCoverBlobUrl(album.id.value)
            case ("remote", album) =>
              Observable(Requests.Urls.albumCover(album))
          }),
      ),
      label(
        cls := "AlbumCoverTitle",
        metaDataHandler.map(_.data.title),
      ),
      label(
        cls := "AlbumCoverAuthor",
        metaDataHandler.map(
          _.data.author.map(_.name).getOrElse("Unknown author"),
        ),
      ),
      onClick.foreach(_ => clickHndlr.runAsyncGetFirst),
    )
}
