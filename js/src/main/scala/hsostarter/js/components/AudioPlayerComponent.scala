package hsostarter.js.components

import hsostarter.js.http.{Requests}
import hsostarter.models.{AlbumData, Track}
import org.scalajs.dom.raw.HTMLAudioElement
import outwatch.dom.dsl._
import outwatch.dom.{Handler, VDomModifier}

case class AudioPlayerState(
  trackMeta: Track,
  albumMeta: AlbumData,
  isPlaying: Boolean,
) {
  def withTogglePlayStatus: AudioPlayerState =
    AudioPlayerState(trackMeta, albumMeta, !isPlaying)
}

final case class AudioPlayerComponent(
  stateHandler: Handler[Option[AudioPlayerState]],
) {
  val node: VDomModifier =
    div(
      cls := "AudioPlayer",
      button(
        cls <-- stateHandler
          .map(_.map(_.isPlaying)).map({
            case Some(true) => "IsPlaying"
            case _          => "IsPaused"
          }).map("AudioPlayer__Controls__Playback " + _),
        onClick(
          stateHandler.map(_.map(_.withTogglePlayStatus)),
        ) --> stateHandler,
      ),
      div(
        cls := "AudioPlayer__MetaData",
        label(
          cls := "AudioPlayer__MetaData__Title",
          stateHandler.map(_.map(_.trackMeta.title).getOrElse("Default")),
        ),
        label(
          cls := "AudioPlayer__MetaData__Author",
          stateHandler.map(
            _.map(_.albumMeta.title).getOrElse("Default source"),
          ),
        ),
      ),
      stateHandler.map({
        case Some(res) =>
          audio(
            cls <-- stateHandler
              .map(_.map(_.isPlaying.toString)).map(_.getOrElse("false")),
            src := Requests.Urls.trackSource(res.trackMeta),
            onDomMount.foreach { element =>
              element.asInstanceOf[HTMLAudioElement].play()
            },
            onDomMount
              .transform(
                _.flatMap(elem => stateHandler.map(state => (elem, state))),
              ).foreach { res =>
                val elem = res._1.asInstanceOf[HTMLAudioElement]
                val isPlaying = res._2.exists(_.isPlaying)
                if (isPlaying && elem.paused) {
                  elem.play()
                } else if (!isPlaying && !elem.paused) {
                  elem.pause()
                }
              },
          )
        case None => audio()
      }),
    )
}
