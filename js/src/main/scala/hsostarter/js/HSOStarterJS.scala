package hsostarter.js

import cats.effect.{ExitCode, IO, IOApp}
import hsostarter.js.data.db.{AlbumSrcSchema, DatabaseManager}
import hsostarter.models.Album

import scala.language.implicitConversions
import outwatch.dom._
import outwatch.dom.dsl._
import monix.execution.Scheduler.Implicits.global

object HSOStarterJS extends IOApp {
  def run(args: List[String]): IO[ExitCode] =
    for {
      localAlbumsHandler <- Handler.create[List[Album]](List())
      localAlbumsSrcHandler <- Handler.create[List[AlbumSrcSchema.schemaTuple]](
        List(),
      )
      dbManager = DatabaseManager(localAlbumsHandler, localAlbumsSrcHandler)
      _            <- dbManager.init()
      topComponent <- TopComponent.init(dbManager)
      _ <- OutWatch.renderReplace(
        "#main",
        div(
          cls := "App",
          ModifierStreamReceiver(ValueObservable(topComponent.node)),
        ),
      )
    } yield ExitCode.Success
}
