package hsostarter.js.data.db

import findexedDB.model.{Col, Collection, ForeignKey, Key}

object AlbumSrcSchema {
  type schemaTuple = (Int, String)
  val collectionName = "albumSrc"

  // list of keys
  def id: Key[Int] =
    Col[Int]("id", Col.IsPrimaryKey) // indicates primary key
  def coverURI: Key[String] = Col[String]("coverUri")
  private val * = (id, coverURI)

  private val $ = Seq(
    ForeignKey(id, AlbumSchema.collectionName, AlbumSchema.id),
  )

  // collection definition
  def collection = Collection[schemaTuple](collectionName)(*)($)
  def getCollectionTitle: String = collection.schema.title
}
