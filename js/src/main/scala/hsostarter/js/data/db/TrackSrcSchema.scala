package hsostarter.js.data.db

import findexedDB.model.{Col, Collection, ForeignKey, Key}

object TrackSrcSchema {
  type schemaTuple = (Int, String)
  val collectionName = "trackSrc"

  // list of keys
  def id: Key[Int] =
    Col[Int]("id", Col.IsPrimaryKey) // indicates primary key
  def audioURI: Key[String] = Col[String]("audioUri")
  private val * = (id, audioURI)

  private val $ = Seq(
    ForeignKey(id, TrackSchema.collectionName, TrackSchema.id),
  )

  // collection definition
  def collection = Collection[schemaTuple](collectionName)(*)($)
  def getCollectionTitle: String = collection.schema.title
}
