package hsostarter.js.data.db

import findexedDB.model.{Col, Collection, ForeignKey, Key}

object TrackSchema {
  type schemaTuple = (Int, Int, Int, String, Int, String)
  val collectionName = "track"

  // list of keys
  def id: Key[Int] =
    Col[Int]("id", Col.IsPrimaryKey) // indicates primary key
  def albumId: Key[Int] = Col[Int]("albumId")
  def position: Key[Int] = Col[Int]("position")
  def title: Key[String] = Col[String]("title")
  def duration: Key[Int] = Col[Int]("duration")
  def source: Key[String] = Col[String]("source")
  private val * = (id, albumId, position, title, duration, source)

  private val $ = Seq(
    ForeignKey(albumId, AlbumSchema.collectionName, AlbumSchema.id),
  )

  // collection definition
  def collection = Collection[schemaTuple](collectionName)(*)($)
  def getCollectionTitle: String = collection.schema.title
}
