package hsostarter.js.data.db

import cats.effect.{IO}
import findexedDB.implementation.IndexedDB.IndexedDBDriver
import findexedDB.model.DatabaseSchema
import hsostarter.js.helpers.IOHelpers
import hsostarter.js.http.Requests
import hsostarter.models.{Album, AlbumData, AuthorData, Id}
import monix.reactive.Observable
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.raw.{URL}
import outwatch.dom.Handler
import cats.implicits._

case class DatabaseManager(
  albumsListHandler: Handler[List[Album]],
  albumsSrcHandler: Handler[List[AlbumSrcSchema.schemaTuple]],
) {
  val schema: DatabaseSchema = DatabaseSchema(
    "cachedtunes",
    4,
    Seq(
      AuthorSchema.collection.schema,
      AlbumSchema.collection.schema,
      AlbumSrcSchema.collection.schema,
      TrackSchema.collection.schema,
      TrackSrcSchema.collection.schema,
    ),
  )

  implicit val driver: IndexedDBDriver = new IndexedDBDriver(schema)

  def init(): IO[Unit] =
    for {
      _ <- driver.run()
      _ <- loadAlbumsToStream()
    } yield ()

  object Query {
    def addAlbum(album: Album): IO[Unit] =
      for {
        blob <- Requests.observableToIO(
          Requests.getAlbumCover(Observable(album)),
        )
        dataUri <- Requests.blobToDataUri(blob)
        authorExists <- AuthorSchema.collection.query
          .exists[AuthorSchema.schemaTuple](_._1 == album.authorId.value)
        _ <-
          if (authorExists) {
            IO.unit
          } else {
            AuthorSchema.collection.query += (album.authorId.value, album.data.author
              .map(_.name).get, "none")
          }
        _ <-
          AlbumSchema.collection.query += (album.id.value, album.authorId.value, album.data.title, album.data.coverSrc)
        _ <- AlbumSrcSchema.collection.query += (album.id.value, dataUri)
        tracks <- Requests
          .observableToIO(Requests.getTracks(Observable(album))).map(_._2)
        tracksAsTuples: List[TrackSchema.schemaTuple] = tracks.map(t =>
          (
            t.id.value,
            t.albumId.value,
            t.position,
            t.title,
            t.duration,
            t.source,
          ),
        )
        _ <- TrackSchema.collection.query ++= tracksAsTuples
        _ <- loadAlbumsToStream()
      } yield ()

    def hasAlbum(albumOption: Option[Album]): IO[Boolean] = albumOption match {
      case Some(album) =>
        AlbumSchema.collection.query
          .filter[AlbumSchema.schemaTuple](_._1 == album.id.value)
          .map(_.nonEmpty)
      case None => IO(false)
    }

    def getAlbumCoverBlobUrl(albumId: Int): Observable[String] =
      for {
        albumCoverUri <- albumsSrcHandler
          .map(_.find(al => al._1 == albumId).map(_._2)).map(
            _.getOrElse("about:blank"),
          )
      } yield URL.createObjectURL(Requests.dataUriToBlob(albumCoverUri))
  }

  private def loadAlbumsToStream(): IO[Unit] =
    getAlbumsAndAuthorsFromStorage.flatMap { result =>
      val (authors, albums, albumsSrc) = result
      updateAlbums(
        albums.map { album =>
          val (albumId, authorId, albumTitle, albumUri) = album
          val author = authors.find(_._1 == authorId)
          Album(
            Id(albumId),
            Id(authorId),
            AlbumData(
              albumTitle,
              "ser",
              author.map { case (_, name, avatarSrc) =>
                AuthorData(name, avatarSrc)
              },
            ),
          )
        },
      )
      updateAlbumsSrc(albumsSrc)
    }

  private def getAlbumsAndAuthorsFromStorage: IO[
    (
      List[AuthorSchema.schemaTuple],
      List[AlbumSchema.schemaTuple],
      List[AlbumSrcSchema.schemaTuple],
    ),
  ] = {
    val allAuthorsIO = AuthorSchema.collection.query
      .all[AuthorSchema.schemaTuple]()
    val allAlbumsIO = AlbumSchema.collection.query
      .all[AlbumSchema.schemaTuple]()
    val allAlbumsSrcIO = AlbumSrcSchema.collection.query
      .all[AlbumSrcSchema.schemaTuple]()

    for {
      allAuthors   <- allAuthorsIO
      allAlbums    <- allAlbumsIO
      allAlbumsSrc <- allAlbumsSrcIO
    } yield (allAuthors, allAlbums, allAlbumsSrc)
  }

  private def updateAlbums(albums: List[Album]): IO[Unit] =
    IOHelpers
      .futureToIO(
        albumsListHandler.onNext(albums),
      ).void

  private def updateAlbumsSrc(
    albumsSrc: List[AlbumSrcSchema.schemaTuple],
  ): IO[Unit] =
    IOHelpers
      .futureToIO(
        albumsSrcHandler.onNext(albumsSrc),
      ).void
}
