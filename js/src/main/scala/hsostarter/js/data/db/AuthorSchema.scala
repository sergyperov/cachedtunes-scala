package hsostarter.js.data.db

import findexedDB.model.{Col, Collection, Key}

object AuthorSchema {
  type schemaTuple = (Int, String, String)
  val collectionName = "author"

  // list of keys
  def id: Key[Int] =
    Col[Int]("id", Col.IsPrimaryKey) // indicates primary key
  def name: Key[String] = Col[String]("name")
  def avatarURI: Key[String] = Col[String]("avatarUri")
  private val * = (id, name, avatarURI)

  private val $ = Seq()

  // collection definition
  def collection = Collection[schemaTuple](collectionName)(*)($)
  def getCollectionTitle: String = collection.schema.title
}
