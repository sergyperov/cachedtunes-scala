package hsostarter.js.data.db

import findexedDB.model.{Col, Collection, ForeignKey, Key}
import hsostarter.models.Album
import org.scalajs.dom.raw.Blob

object AlbumSchema {
  type schemaTuple = (Int, Int, String, String)
  val collectionName = "album"

  // list of keys
  def id: Key[Int] =
    Col[Int]("id", Col.IsPrimaryKey) // indicates primary key
  def authorId: Key[Int] = Col[Int]("authorId")
  def title: Key[String] = Col[String]("title")
  def src: Key[String] = Col[String]("src")
  private val * = (id, authorId, title, src)

  private val $ = Seq(
    ForeignKey(authorId, AuthorSchema.collectionName, AuthorSchema.id),
  )

  // collection definition
  def collection = Collection[schemaTuple](collectionName)(*)($)
  def getCollectionTitle: String = collection.schema.title
}
