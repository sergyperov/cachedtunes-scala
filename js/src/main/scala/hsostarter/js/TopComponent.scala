package hsostarter.js

import cats.effect.IO
import hsostarter.js.components.{
  AlbumCoverComponent,
  AlbumsGridComponent,
  AudioPlayerComponent,
  AudioPlayerState,
  HeaderComponent,
  TrackListComponent,
}
import monix.reactive.Observable
import outwatch.dom.{VDomModifier, VNode}
import outwatch.dom._
import outwatch.dom.dsl._
import cats.implicits._
import hsostarter.js.components.TrackListComponent.TrackListProps
import hsostarter.js.data.db.DatabaseManager
import hsostarter.js.http.Requests
import hsostarter.models.{Album, AlbumData, Track}
import monix.execution.Scheduler.Implicits.global
import io.circe.parser.parse
import org.scalajs.dom.document
import outwatch.http.Http
import outwatch.http.Http.Request

import scala.util.Try

final case class TopComponent(
  header: Observable[VDomModifier],
  albumObservable: Observable[VDomModifier],
  trackListObservable: Observable[VDomModifier],
) {

  def node: Observable[VDomModifier] =
    for {
      headerNode     <- header
      albumsGridNode <- albumObservable
      trackListNode  <- trackListObservable
    } yield
      VDomModifier(
        headerNode,
        div(
          cls := "TopWrapper",
          albumsGridNode,
          trackListNode,
        ),
      )

}

object TopComponent {
  def getAlbumNodes(
    remoteAlbumsStream: Observable[List[Album]],
    localAlbumsStream: Observable[List[Album]],
    gridModeStream: Observable[String],
    dbManager: DatabaseManager,
    currentAlbumHandler: Handler[Option[Album]],
  ): Observable[List[AlbumCoverComponent]] =
    for {
      remoteAlbums <- remoteAlbumsStream
      localAlbums  <- localAlbumsStream
      albums <- gridModeStream.map({
        case "local"  => localAlbums
        case "remote" => remoteAlbums
      })
//        if (gridMode == "local") localAlbumsStream else remoteAlbumsStream
    } yield
      albums.map(album =>
        AlbumCoverComponent(
          Observable(album),
          gridModeStream,
          dbManager,
          currentAlbumHandler,
        ),
      )

  def getTrackList(
    currentAlbumHandler: Handler[Option[Album]],
  ): Observable[Option[TrackListProps]] =
    currentAlbumHandler.flatMap {
      case Some(currentAlbum) =>
        Requests.getTracks(Observable(currentAlbum)).map(Some(_))
      case None => Observable(None)
    }

  def init(
    dbManager: DatabaseManager,
  ): IO[TopComponent] =
    for {
      searchString       <- Handler.create[String]("")
      currentAlbum       <- Handler.create[Option[Album]](None)
      audioSourceHandler <- Handler.create[Option[AudioPlayerState]](None)
      trackListProps     <- IO(getTrackList(currentAlbum))
      //audioPlayerComponent <- IO(AudioPlayerComponent(audioSourceHandler))
      localAlbums <- IO(dbManager.albumsListHandler)
      remoteAlbums <- IO(
        Requests.getAlbums(searchString),
      )
      gridDisplayMode <- IO(
        searchString.zip(localAlbums).map { case (q, l) =>
          if (q.isEmpty && l.nonEmpty) "local" else "remote"
        },
      )
      albumNodes <- IO(
        getAlbumNodes(
          remoteAlbums,
          localAlbums,
          gridDisplayMode,
          dbManager,
          currentAlbum,
        ),
      )
      header <- IO(HeaderComponent(searchString))
      albumsGridNode <- IO(
        AlbumsGridComponent(
          albumNodes,
          searchString
            .zip(localAlbums).map({ case (q, l) =>
              if (q.nonEmpty) {
                "Search results"
              } else if (l.nonEmpty) {
                "Saved Music Collection"
              } else {
                "Top Albums"
              }
            }),
          dbManager,
          gridDisplayMode,
        ),
      )
      trackListNode <- IO(
        TrackListComponent(trackListProps, audioSourceHandler, dbManager),
      )
    } yield
      TopComponent(
        header.node,
        albumsGridNode.node,
        trackListNode.node,
      )
}
