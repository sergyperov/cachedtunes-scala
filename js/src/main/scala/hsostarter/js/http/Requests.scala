package hsostarter.js.http

import cats.effect.{ContextShift, IO}
import hsostarter.models.{Album, PagerData, SearchQuery, Track, TracksQuery}
import monix.reactive.Observable
import outwatch.http.Http
import outwatch.http.Http.Request
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.{document, FileReader}
import org.scalajs.dom.raw.{Blob, BlobPropertyBag}
import io.circe.parser.parse
import io.circe.syntax._
import monix.catnap.SchedulerEffect
import monix.execution.Scheduler
import org.scalajs.dom.window.atob
import scala.scalajs.js
import scala.scalajs.js.typedarray.Uint8Array

/**
 * Here all web requests are stored
 */

object HttpRequestsLib {
  def requests(
    req: Observable[Request],
  ): Observable[List[Album]] =
    for {
      response <- Http.post(req)
      result <- Observable.fromEither(
        parse(response.xhr.responseText).flatMap(_.as[List[Album]]),
      )
    } yield result
}

object Requests {
  private lazy val protocolHost = {
    val origin = document.location.origin.asInstanceOf[String]

    if (origin.contains("localhost"))
      "http://localhost:8080"
    else
      origin
  }

  def getAlbums(query: Observable[String]): Observable[List[Album]] = {
    val req = query.map(q =>
      Request(
        url = s"$protocolHost/search",
        data = SearchQuery(q, PagerData(0, 10)).asJson.noSpaces,
      ),
    )

    for {
      response <- Http.post(req)
    } yield
      parse(response.xhr.responseText)
        .flatMap(_.as[List[Album]]).getOrElse(List())
  }

  def getTracks(
    albumStream: Observable[Album],
  ): Observable[(Album, List[Track])] = {
    val req = albumStream
      .map(_.id).map(albumId =>
        Request(
          url = s"$protocolHost/getTracks",
          data = TracksQuery(albumId).asJson.noSpaces,
        ),
      )

    for {
      response <- Http.post(req)
      album    <- albumStream
    } yield {
      val trackList = parse(response.xhr.responseText)
        .flatMap(_.as[List[Track]]).getOrElse(List())

      (album, trackList)
    }
  }

  def getAlbumCover(
    albumStream: Observable[Album],
  ): Observable[Blob] = {
    val req = albumStream.map(album =>
      Request(
        url = Requests.Urls.albumCover(album),
        responseType = "blob",
      ),
    )
    for {
      response <- Http.get(req)
    } yield
      new Blob(
        scalajs.js.Array(response.response),
        BlobPropertyBag("image/png"),
      )
  }

  def observableToIO[T](
    observable: Observable[T],
  )(implicit s: Scheduler): IO[T] = {
    implicit val cs: ContextShift[IO] =
      SchedulerEffect.contextShift[IO](s)(IO.ioEffect)

    IO.fromFuture(IO(observable.runAsyncGetFirst(s).map(_.get)(s)))(cs)
  }

  def blobToDataUri(blob: Blob): IO[String] = {
    val fileReader = new FileReader();

    IO.async { cb =>
      fileReader.onload = e => {
        cb(Right(fileReader.result.asInstanceOf[String]))
      }
      fileReader.readAsDataURL(blob)
    }
  }

  def dataUriToBlob(dataUri: String): Blob = {
    val arr = dataUri.split(',')
    val mime =
      ":(.*?);".r.findFirstMatchIn(arr(0)).map(_.group(1)).getOrElse("")

    val bstr = atob(arr(1))
    val n = bstr.length
    val u8arr = new Uint8Array(n)
    for (i <- 0 until n)
      u8arr.update(i, bstr.codePointAt(i).toShort)

    new Blob(js.Array(u8arr), BlobPropertyBag(mime));
    //println(URL.createObjectURL(blob))
  }

  object Urls {
    def albumCover(album: Album): String = album.data.coverSrc

    def trackSource(track: Track): String = track.source
  }
}
