package hsostarter.js.helpers

import cats.effect.{ContextShift, IO}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext

object IOHelpers {
  implicit val contextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  def futureToIO[T](future: Future[T]): IO[T] =
    IO.fromFuture(IO(future))
}
