import IOJSSupport.IOJsDomTestSuite
import cats.effect._
import hsostarter.js.components.AlbumCoverComponent
import hsostarter.js.data.db.{AlbumSrcSchema, DatabaseManager}
import hsostarter.models.{Album, AlbumData, AuthorData, Id}
import utest._
import outwatch.dom.Handler
import monix.reactive.Observable
import org.scalajs.dom.document

object AlbumCoverTest extends IOJsDomTestSuite {
  val sampleAlbumData: AlbumData =
    AlbumData(
      "Some Album",
      "http://example.com/cover.jpg",
      Some(AuthorData("Some Author", "png")),
    )

  def getDbManager: IO[DatabaseManager] =
    for {
      localAlbumsHandler <- Handler.create[List[Album]](List())
      localAlbumsSrcHandler <- Handler.create[List[AlbumSrcSchema.schemaTuple]](
        List(),
      )
      dbManager = DatabaseManager(localAlbumsHandler, localAlbumsSrcHandler)
    } yield dbManager

  val tests: Tests = Tests {
    test("AlbumCover renders correct properties") {
      for {
        metaDataHandler <- Handler.create[Album](
          Album(Id(1), Id(1), sampleAlbumData),
        )
        currentAlbumHandler <- Handler.create[Option[Album]](None)
        dbManager           <- getDbManager
        node = AlbumCoverComponent(
          metaDataHandler,
          Observable("remote"),
          dbManager,
          currentAlbumHandler,
        ).node
        _ <- renderAndSleep(node)
      } yield {
        val albumCover = document.body.firstElementChild.querySelector(
          ".AlbumCover",
        )

        val image = albumCover.querySelector(".AlbumCoverImg")
        val imageSrc = image.getAttribute("src")
        val imageAlt = image.getAttribute("alt")
        val title = albumCover.querySelector(".AlbumCoverTitle").innerHTML
        val author = albumCover.querySelector(".AlbumCoverAuthor").innerHTML

        imageSrc ==> "http://example.com/cover.jpg"
        imageAlt ==> "Some Album"
        title ==> "Some Album"
        author ==> "Some Author"
      }
    }
  }
}
