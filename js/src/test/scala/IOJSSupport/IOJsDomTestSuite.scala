package IOJSSupport

import cats.effect.{ContextShift, IO, Timer}
import org.scalajs.dom.document
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom
import outwatch.dom.{OutWatch, VNode}
import utest._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.scalajs.js

abstract class IOJsDomTestSuite extends TestSuite {
  protected def makeExecutionContext(): ExecutionContext =
    ExecutionContext.global

  protected def timeout: FiniteDuration = 10.seconds

  protected def allowNonIOTests: Boolean = false

  protected lazy val executionContext: ExecutionContext = makeExecutionContext()

  implicit def ioContextShift: ContextShift[IO] =
    IO.contextShift(executionContext)

  implicit def ioTimer: Timer[IO] = IO.timer(executionContext)

  override def utestWrap(path: Seq[String], runBody: => Future[Any])(implicit
    ec: ExecutionContext,
  ): Future[Any] = {
    document.body.innerHTML = ""

    // prepare body with <div id="root"></div>
    val root = document.createElement("div")
    root.id = "root"
    document.body.appendChild(root)

    // Shadow the parameter EC with our EC
    implicit val ec: ExecutionContext = this.executionContext
    runBody.flatMap {
      case io: IO[Any]              => io.timeout(timeout).unsafeToFuture()
      case other if allowNonIOTests => Future.successful(other)
      case other =>
        throw new RuntimeException(
          s"Test body must return an IO value. Got $other",
        )
    }
  }

  def renderAndSleep(node: VNode): IO[Unit] = for {
    _ <- OutWatch.renderInto("#root", node)
    _ <- IO.sleep(3.second)
  } yield ()

  def createDomEvent(
    event: String,
    bubbles: Boolean = true,
    cancelable: Boolean = false,
  ): dom.Event = {
    val eventInit =
      js.Dictionary("bubbles" -> bubbles, "cancelable" -> cancelable)

    js.Dynamic
      .newInstance(js.Dynamic.global.Event)(
        event,
        eventInit,
      ).asInstanceOf[dom.Event]
  }
}
